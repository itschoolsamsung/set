import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        TreeSet<String> t = new TreeSet<>();
        t.add("xxx");
        t.add("yyy");
        t.add("aaa");
        System.out.println(t);

        class Rect implements Comparable {
            int width;
            int height;

            Rect(int width, int height) {
                this.width = width;
                this.height = height;
            }

            int area() {
                return width * height;
            }

            @Override
            public String toString() {
                return String.format("%dx%d=%d", width, height, area());
            }

            @Override
            public int compareTo(Object o) {
                return Integer.compare( area(), ((Rect)o).area() );
            }
        }

        TreeSet<Rect> r = new TreeSet<>();
        r.add(new Rect(1, 10));
        r.add(new Rect(3, 2));
        r.add(new Rect(5, 1));
        System.out.println(r);
    }
}
